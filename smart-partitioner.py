import psutil
import inquirer
import re
from simple_chalk import chalk
from pyfiglet import figlet_format

f = figlet_format
errors = inquirer.errors
yellow = chalk.yellow
red = chalk.red
green = chalk.green
diskpart = psutil.disk_partitions
diskusage = psutil.disk_usage
disklist = diskpart(False)
disklist = ["/dev/sda2", "/dev/sda2", "/dev/sda2"]
partition_list = []

# All the converter functions


def to_MB(inbytes):
    to_mb = float(inbytes/1024/1024)
    return to_mb


def to_GB(inbytes):
    to_gb = float(inbytes/1024/1024/1024)
    return to_gb


def to_percent(num, total):
    percent = num*100/total
    return percent


def to_percent_of_num(perc, num):
    percent_of_num = perc*num/100
    return percent_of_num

# Validation for number of partition to input
def nonevalidation(answers, current):
    if current == 'null' or current == '':
        raise errors.ValidationError(
            '', reason='Please enter atleast 1 number of part!')
    if not re.match('^[1-9]+[0-9]*$', current):
        raise errors.ValidationError(
            '', reason='Please enter number from 1 to n.')
    return True

# Gets total of partition from list of partition
def total_of_part_list(partition_list):
    total_size = 0

    for part_list in partition_list:
        mnt = part_list['mnt']
        total_size = to_MB(diskusage(mnt).free) + total_size

    return total_size

# Excludes partition and returns other
def exclude(answers, current):
    for excluded in current:
        partition_list.remove(excluded)
    # print(partition_list)
    total_size = total_of_part_list(partition_list)
    totalofpartsize = f"Total of part size: {green(round(total_size))}(rounded) MB {green(total_size)}(float) MB"
    print(f"\n{totalofpartsize}")
    return True

# Validation for user required amount of space is available in the disk or its exceeding
def spaceavailable(answers, current):
    total_size = total_of_part_list(partition_list)
    # print(total_size)
    if int(total_size) >= int(current):
        return True

# Ask user for Number of part to create and Size of that part
total_size = 0

for device in disklist:
    partition = device
    mnt = '/'
    mnt_in_mb = to_MB(diskusage(mnt).free)
    partition_list.append(
        {"partition": partition, "mnt": mnt, 'free(mb)': int(mnt_in_mb)})


# Ask questions to user and get user input to act on partition
questions = [
    inquirer.Checkbox(
        'select_partition', message='Select partition to exclude? ', choices=partition_list, validate=exclude),
    inquirer.Text('num_part', message="Number of partitions to create?",
                  default='1', validate=nonevalidation),
    inquirer.Text(
        'size', message="What will be the partition size(MB)?", validate=spaceavailable)
]

# Ask confirmation to proceed
confirmation = [
    inquirer.Confirm(
        'continue', message='Do you still want to continue creating partition?', default=False)
]

# Greetings and prompt for questions
print(yellow(f('Welcome to Fast Partitioner.', font="small")))
print(red(f"Note: Partition size will be approx but close to your number specified. So don't panic. \n"))
exclude_part = inquirer.prompt(questions)

# for excluded in exclude_part['select_partition']:
#     partition_list.remove(excluded)

# total_size = 0

# for part_list in partition_list:
#     mnt = part_list['mnt']
#     total_size = to_MB(diskusage(mnt).free) + total_size

# Calculates percent and returns percent for user to know how much space spending from each partition
total_size = total_of_part_list(partition_list)

totalofpartsize = f"Total of part size: {green(total_size)} MB"
print(totalofpartsize)
percentofuser = to_percent(
    float(exclude_part['size'])*int(exclude_part['num_part']), total_size)
print(f"{round(percentofuser)}% of disk space will be reduced from each partition.")

sumofeachpart = 0

for part_list in partition_list:
    mnt = part_list['mnt']
    perpart = to_MB(diskusage(mnt).free)
    shrinkamnt = to_percent_of_num(percentofuser, perpart)
    partname = yellow(f"{part_list['partition']}")
    roundshrink = red(f'{round(shrinkamnt)}')
    roundperpart = green(f'{round(perpart)}')
    print(f"{partname} will be shrinked by {roundshrink} MB which is {round(percentofuser)}% out of {roundperpart} MB of free space")
    sumofeachpart = to_percent_of_num(percentofuser, perpart) + sumofeachpart

askconfirm = inquirer.prompt(confirmation)

# print(askconfirm)

# if confirmed PROCEEDS else EXITS
if askconfirm['continue'] == False:
    exit()

divided = sumofeachpart/int(exclude_part['num_part'])

print(f"Divided end result is {round(divided)} MB")
