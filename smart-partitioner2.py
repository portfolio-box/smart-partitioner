import psutil
import inquirer
from simple_chalk import chalk
from pyfiglet import figlet_format

f = figlet_format
yellow = chalk.yellow(f('Welcome to Smart Partitioner.', font="small"))
red = chalk.red
green = chalk.green
diskpart = psutil.disk_partitions
diskusage = psutil.disk_usage
disklist = diskpart(False)
disklist = ["/dev/sda2", "/dev/sda2", "/dev/sda2"]

partition_list = []


def mb(inbytes):
    inmb = float(inbytes/1024/1024)
    return inmb


def gb(inbytes):
    ingb = float(inbytes/1024/1024/1024)
    return ingb


def calc_percent(num, total):
    calc = num*100/total
    return calc


def calc_percent_of_num(perc, num):
    calc = perc*num/100
    return calc


# Ask user for Number of part to create and Size of that part


total_size = 0

for device in disklist:
    partition = device
    mnt = '/'
    # partition = device.device
    # mnt = device.mountpoint
    mnt_in_mb = mb(diskusage(mnt).free)
    partition_list.append(
        {"partition": partition, "mnt": mnt, 'free(mb)': int(mnt_in_mb)})
    # print(partition)
    # print(mnt)
    # print(gb(diskusage(mnt).free))

# print(f"Total of part size: {total_size}")


# print(calc_percent(7202))

# print(calc_percent_of_num(7202))

# print(calc_percent_of_num(7202)*3)

questions = [
    inquirer.Text('size', message="What will be the partition size(MB)?"),
    inquirer.Text('part_num', message="Number of partitions to create?"),
    inquirer.Checkbox(
        'select_partition', message='Select partition to exclude? ', choices=partition_list)
]


print(yellow)
exclude_part = inquirer.prompt(questions)
# print(exclude_part['select_partition'])

for excluded in exclude_part['select_partition']:
    partition_list.remove(excluded)

# Take user input on how much part
# Take user input on how many part
# Create percent with provided GB and total Selected part GB
# Take that amount of percent size from every part and create part

total_size = 0

for part_list in partition_list:
    # print(part_list['partition'])
    # print(part_list['mnt'])
    mnt = part_list['mnt']
    # print(mb(diskusage(mnt).free))
    total_size = mb(diskusage(mnt).free) + total_size
    # print(part_list.partition)
    # print(part_list.mnt)

print(f"{red('Total of part size:')} {total_size} {green('MB')}")
percentofuser = calc_percent(float(exclude_part['size']), total_size)
print(f"{calc_percent(float(exclude_part['size']), total_size)}")

sumofeachpart = 0

for part_list in partition_list:
    mnt = part_list['mnt']
    perpart = mb(diskusage(mnt).free)
    print(calc_percent_of_num(percentofuser, perpart))
    sumofeachpart = calc_percent_of_num(percentofuser, perpart) + sumofeachpart

print(sumofeachpart)


# print(calc_percent(userin))
# print(diskpart())
# print(int(diskusage('/').percent))
