# Smart Partitioner

Python based drive partitioner which ask questions regarding the disk and number/size of partition to tell how much you have to shrink from each partition to create a new partition of specified size.